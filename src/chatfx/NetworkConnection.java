/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatfx;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.Consumer;

/**
 *
 * @author Stefano
 */
public abstract class NetworkConnection {
    
  //  private ThreadConnessione threadConn = new ThreadConnessione();
    private ThreadConnessione threadConn;
    private Consumer<Serializable> messaggioInArrivo;
    protected abstract boolean isServer();
    protected abstract String getIp();
    protected abstract int getPort();
    
    
    public NetworkConnection(Consumer<Serializable> messaggioInArrivo) {
        this.messaggioInArrivo = messaggioInArrivo;
        
        threadConn = new ThreadConnessione();
        // serve a far capire alla jvm che questo è un thread dell'utente
        threadConn.setDaemon(true);
    }
    

    
    public void startConnection() throws Exception {
        threadConn.start();
    }
    
    public void stopConnection() throws Exception {
        threadConn.serverSocket.close();
        threadConn.socketClient.close();
        threadConn.getIn().close();
        threadConn.getOut().close();
    }
    
    public void send(Serializable data) throws Exception {
    	threadConn.getOut().writeObject(data);
    }
    
    private class ThreadConnessione extends Thread{
        
        private ServerSocket serverSocket;
        private Socket socketClient;
        private ObjectInputStream in;
        private ObjectOutputStream out;
                
        @Override
        public void run() {
            try {
                if (isServer()) {
                	messaggioInArrivo.accept("Creazione Server");
                    serverSocket = new ServerSocket(getPort(), 100);
                    messaggioInArrivo.accept("In attesa di una connessione del client");
                    socketClient = serverSocket.accept();
                    messaggioInArrivo.accept("Client connesso");
                } else {
                    messaggioInArrivo.accept("Creazione Client in corso");
                    socketClient = new Socket(getIp(), getPort());
                    messaggioInArrivo.accept("Client creato e connesso al server");
                }
                /*
                RISOLTO BUG---Con questo tipo di stream bisogna inizializzare prima
                              l'out poi dopo .flush si inizializza l'in
                */
                out = new ObjectOutputStream(socketClient.getOutputStream());
                out.flush();
                in = new ObjectInputStream(socketClient.getInputStream());
                // per non aver problemi con la connessione
                socketClient.setTcpNoDelay(true);
                               
                while(true) {
                    Serializable data = (Serializable) in.readObject();
                    messaggioInArrivo.accept(data);
                }
                
            } catch (Exception e){
                messaggioInArrivo.accept("Connessione Chiusa");

            }
        } 
        
        public ServerSocket getServerSocket()
		{
			return serverSocket;
		}

		public void setServerSocket(ServerSocket serverSocket)
		{
			this.serverSocket = serverSocket;
		}

		public Socket getSocketClient()
		{
			return socketClient;
		}

		public void setSocketClient(Socket socketClient)
		{
			this.socketClient = socketClient;
		}

		public ObjectInputStream getIn()
		{
			return in;
		}
		
		public ObjectOutputStream getOut()
		{
			return out;
		}


		public void setIn(ObjectInputStream in)
		{
			this.in = in;
		}

		public void setOut(ObjectOutputStream out)
		{
			this.out = out;
		}

        
        
    
    }
    
    
}


