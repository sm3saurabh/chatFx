/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatfx;

import java.io.Serializable;
import java.util.function.Consumer;

/**
 *
 * @author Stefano
 */
public class Client extends NetworkConnection {
        
    private int port;
    private String ip;
    private String nomeClient;

    public Client(int port, String ip, String nomeUser, Consumer<Serializable> messaggioInArrivo) {
        super(messaggioInArrivo);
        this.port = port;
        this.ip = ip;
        this.nomeClient = nomeUser;
    }
    @Override
    protected boolean isServer() {
        return false;
    }

    @Override
    protected String getIp() {
        return ip;
    }

    @Override
    protected int getPort() {
        return port;
    }

    public String getNomeClient() {
        return nomeClient;
    }
}
